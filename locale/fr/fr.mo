��          �   %   �      0  6   1  '   h  -   �  *   �  !   �  :     ,   F  )   s  �   �     %  "   E     h  (   |     �     �  U   �  $   0     U     k     s  �   �  &     &   E  +  l  G   �  -   �  H     /   W  -   �  G   �  =   �  F   ;	  �   �	  &   (
  .   O
     ~
  .   �
     �
     �
  Y      $   Z  +     	   �     �  �   �  *   �  0   �                                                                        	                     
                                  Actual date/time : %s. Empty disable maintenance mode. Admin login page are always accessible. Allow only super administrator to admin page. Bad url, you must review the redirect url. Date / time for maintenance mode. Default message was translated according to user language. Disable public part for administrator users. Disable token emailing during maintenance Enter complete url only (with http:// or https://). {LANGUAGE} was replace by user language (ISO format if exist in this installation). Even for Superadministrator(s). In minutes or with english string. Maintenance message Only for LimeSurvey 3.0.0 and up version Show warning message delay. This disable your access This website close for maintenance at {DATEFORMATTED} (in {intval(MINUTES)} minutes). This website is in maintenance mode. Url to redirect users Warning Warning message. You can use Expression manager : %s was replaced by date in user language format, %s by date in %s format and %s by number of minutes before maintenance. You must activate renderMessage plugin You must download renderMessage plugin PO-Revision-Date: 2019-02-12 14:35+0100
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Poedit 2.2.1
Language: fr
Project-Id-Version: maintenanceMode
POT-Creation-Date: 
Last-Translator: 
Language-Team: 
 Date et heure actuelle : %s. Vide pour désactiver le mode maintenance. La page de connexion est toujours accessible. Ne permettre l'accès à l'administration que aux super-administrateurs. L'adresse internet de redirection est invalide. Date et heure du passage en mode maintenance. Le message par défaut sera traduit selon la langue de l‘utilisateur. Désactiver l'accès aux pages publiques aux administrateurs. Désactiver l'envoie des courriels d'invitations durant la maintenance Entrez une adresse complète (avec http:// ou https://). {LANGUAGE} sera remplacé par la langue de l'utilisateur (au format ISO si elle existe dans l'installation). Même pour les super-administrateur(s) En minutes ou avec un date en langue anglaise. Message de maintenance Uniquement pour LimeSurvey 3.0.0 ou supérieur Montrer le message d'alerte. Ceci désactivera votre accès Ce site internet sera en maintenance à {DATEFORMATTED} (dans {intval(MINUTES)} minutes). Ce site internet est en maintenance. Adresse url de redirection des utilisateurs Attention Message d'alerte. Vous pouvez utiliser le gestionnaire d'expressions. %s sera remplacé par la date formatée en fonction de la langue de l'utilisateur, %s par la date au format %s et %s par le nombre de minutes avant la maintenance. Vous devez activer le plugin renderMessage Vous devez télécharger le plugin renderMessage 